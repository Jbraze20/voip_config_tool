
# TODO

- Report bugs here

- Todo list
	-  [ ]  index.html fixes
	 - [ ] `getMacAddress(barcode)`
	 - [ ] `getMacAddress_Bulk(barcodes)`
	 - [ ] `uploadToServer(file)`

### index.html
- Remove the result text area when ready for deployment as its only for testing purposes.
- Remove 'Build Config File' button when result text area is removed.
- Change 'Save File' button to 'Upload File' button.

###  utilityFunction.js

#### - `getMacAddress(barcode)`
- Should query the MongoDB database "voip_pb_data" through [mLab Database](https://mlab.com/databases/voip_pb_data) using the provided barcode for a device to get its MAC address.
- Suggested libraries to use: [Mongoose](http://mongoosejs.com/) or [MongoClient](https://mongodb.github.io/node-mongodb-native/api-generated/mongoclient.html)
- MongoDB url: `mongodb://tourtech:roundRock1723!@ds117701.mlab.com:17701/voip_pb_data`
- **Username:** tourtech, **Password:** roundRock1723!
- Collection: Barcode-MAC
	- Documents are set up as follows
		- {
				Barcode: #####,
				MAC: ############
			}

#### - `getMacAddress_Bulk(barcodes)`
- Should perform the same functionality as getMacAddress(barcode), but on an array of barcodes, returning an array of MAC addresses in the same order as the barcodes were given.
- Recommend just looping call of getMacAddress(barcode)

### fileMaker.js
#### - `uploadToServer(file)`
- Should upload the provided file to the VOIP phone ftp server used by TOURtech.
- File will either end in .cfg for Yealink or.xml for Cisco phones
	- The file creation functionality should already be present in the `saveTextAsFile(mode)` function 
- End game will replace the 'Save File' button with a 'Upload Profile' button that will call the `saveTextAsFile(mode)`function, build the file(s),  then call `uploadToServer(file)`.