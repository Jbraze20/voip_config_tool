#include <stdio.h>
#include <string.h>
 
#define bufSize 60
 
int main(int argc, char *argv[])
{
  FILE* fp;
  FILE* dataFile;
  char buf[bufSize];
  if (argc != 2)
  {
    fprintf(stderr, "Usage: %s <soure-file>\n", argv[0]);
    return 1;
  }
  if ((fp = fopen(argv[1], "r")) == NULL)
  { /* Open source file. */
    perror("fopen source-file");
    return 1;
  }
  if ((dataFile = fopen("MACBarcode_data.txt", "a")) == NULL)
  { /* Open source file. */
    perror("fopen write-file");
    return 1;
  }
 
  char barcode[25];
  char mac[25];
  char entry[55];
  while (fgets(buf, sizeof(buf), fp) != NULL)
  {
    buf[strlen(buf) - 1] = '\0'; // eat the newline fgets() stores
    if (strstr(buf, "barcodeString") != NULL) {
        memset(entry, '\0', sizeof entry);
        strcat(entry, "{ ");
        memset(barcode, '\0', sizeof barcode);
        memcpy(barcode, buf, strlen(buf)-1);
        strcat(entry, barcode);
        strcat(entry, ", ");
    } else if (strstr(buf, "stencil") != NULL) {
        memset(mac, '\0', sizeof mac);
        memcpy(mac, buf, strlen(buf)-1);
        strcat(entry, mac);
        strcat(entry, " }\n");
        printf("%s\n", entry);
        fputs(entry, dataFile);
    }
    memset(buf, '\0', sizeof buf);
  }
  fclose(fp);
  fclose(dataFile);
  return 0;
}
