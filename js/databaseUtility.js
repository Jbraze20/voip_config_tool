//const MongoClient = require('mongodb').MongoClient;
import * as mongodb from 'mongodb';
const MongoClient = mongodb.MongoClient;
import { readFileSync } from "fs";

const MONGO_URL = 'mongodb://tourtech:roundRock1723!@ds117701.mlab.com:17701/voip_pb_data';

/**
 * Queries the database to retrieve the MAC address associated with the given barcode.
 * 
 * @param {string} barcode the barcode to match to the MAC address
 * @returns {string} the mac address
 */
export function getMacAddress(barcode) {
    MongoClient.connect(MONGO_URL, (err, client) => {  
        if (err) {
            return console.log(err);
        }
        let db = client.db('voip_pb_data');

        // Get the documents collection
        const collection = db.collection('Barcode-MAC');
        let query = { Barcode: barcode };
        let mac = "";
        // Insert some documents
        collection.find(query, {_id: 0, MAC: 1}).toArray(function(err, result) {
            if (err) throw err;
            mac = result[0].MAC;
        });
        client.close();
    });
    return mac;
}

function uploadPhoneData() {
    MongoClient.connect(MONGO_URL, (err, client) => {  
        if (err) {
            return console.log(err);
        }

        let db = client.db('voip_pb_data');

        parseXML("../data/Yealink_T46G_records.xml", db);
        parseXML("../data/Cisco_Trimline_records.xml", db);
        parseXML("../data/Cisco_SingleLine_records.xml", db);
        parseXML("../data/Cisco_4Line_records.xml", db);
        parseXML("../data/Cisco_5Line_records.xml", db);
        client.close();
        
    });
}

function parseXML(filepath, db) {
    let text = readFileSync(filepath, "utf-8");
    let textByLine = text.split("\n");
    let barcode = "";
    let mac = "";
    for (const line of textByLine) {
        if (line.includes("barcodeString")) {
            barcode = line.substring(line.indexOf('\'') + 1, line.lastIndexOf('\''));
        } else if (line.includes("stencil")) {
            mac = line.substring(line.indexOf('\'') + 1, line.lastIndexOf('\''));
            insertDocuments(db, barcode, mac);  
        }
    }
}

function insertDocuments(db, barcode, mac) {
    // Get the documents collection
    const collection = db.collection('Barcode-MAC');
    // Insert some documents
    collection.insert([{Barcode: barcode, MAC: mac}]);
}