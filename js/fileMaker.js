//// TOURtech Constants ////
/** VOIP Phone server proxy */
const PROXY = "tourtech.questblue.com";
const DOMAIN = "TOURtech";
const PRIMARY_DNS = "10.0.0.1";
const SECONDARY_DNS = "8.8.8.8";
const SERVER_URL = "http://www.tourtechsupport.net";


/** Array of built config files */
let configFiles = [];

/** 
 * Individual Setup mode
 * 
 * Creates a config file for the individual device.
 */
function displayOutput() {
    document.getElementById("result").innerHTML = ""; // Clear output area, should be removed when output area is phased out
    let type = document.getElementById("phoneSelect").value;
    let extension = document.getElementById("Extension").value.trim();
    let did = document.getElementById("DID").value.trim();
    let password = document.getElementById("Password").value.trim();
    let barcode = document.getElementById("Barcode").value.trim();
    let mac = getMacAddress(barcode);
    let timeZone = document.getElementById("timeZoneSelect").value.trim();
    let background = document.getElementById("Background").value.trim();
    let name = document.getElementById("Name").value.trim();
        if (extension.length === 0 || password.length === 0 || barcode.length === 0) {
        alert("Please enter a valid Extension, Password, and Barcode/MAC address.");
        return;
    }

    let file = "";
    switch (type) {
    case "Y_T46G":
        const yTimeZone = timeZone.substring(3, 6);
        file = displayYealinkOutput("Individual", extension, did, password, mac, yTimeZone, background, name);
        break;
    case "C_502G":
    case "C_922":
    case "C_301":
    case "C_504G":
    case "C_525G":
    case "C_525G2":
        file = displayCiscoOutput("Individual", extension, did, password, mac, timeZone, background, name, type);
        break;
    }
    document.getElementById("result").innerHTML = file; // Should be removed when output area is phased out
    configFiles.length = 0; // Clear old config files
    configFiles.push(file);
}

/**
 * Bulk Setup Mode
 * 
 * Creates Config files for each device, where the contacts are all other devices.
 */
function displayOutputs_Bulk() {
    document.getElementById("result").innerHTML = ""; // Clear output area, should be removed when outout area is phased out
    let types = document.getElementsByClassName("phoneSelect_Bulk");
    let extensions = document.getElementsByClassName("Extension_Bulk");
    let dids = document.getElementsByClassName("DID_Bulk");
    let passwords = document.getElementsByClassName("Password_Bulk");
    let barcodes = document.getElementsByClassName("Barcode_Bulk");
    let macs = getMacAddress_Bulk(barcodes);
    let timeZone = document.getElementById("timeZoneSelect_Bulk").value;
    let backgrounds = document.getElementsByClassName("Background_Bulk");
    let names = document.getElementsByClassName("Name_Bulk");
    for (let i = 0; i < extensions.length; i++) {
        if (extensions[i].value.length === 0 || passwords[i].value.length === 0 || barcodes[i].value.length === 0) {
        alert("Please enter all valid Extensions, Passwords, and Barcode/MAC addresses.");
        return;
        }
    }
    
    let files = [];
    for (let i = 0; i < types.length; i++) {
        switch (types[i].value) {
        case "Y_T46G":
            const yTimeZone = timeZone.substring(3, 6);
            files.push(displayYealinkOutput("Bulk", extensions[i].value, dids[i].value, passwords[i].value, macs[i].value, yTimeZone, backgrounds[i].value, names[i].value, i));
            break;
        case "C_502G":
        case "C_922":
        case "C_301":
        case "C_504G":
        case "C_525G":
        case "C_525G2":
            files.push(displayCiscoOutput("Bulk", extensions[i].value, dids[i].value, passwords[i].value, macs[i].value, timeZone, backgrounds[i].value, names[i].value, types[i].value, i));
            break;
        }
    }

    configFiles.length = 0; // Clear old config files
    for (let i = 0; i < files.length; i++) {
        // The 3 lines pertainign to the output area should be removed when outout area is phased out
        document.getElementById("result").innerHTML += "##   Device " + (i + 1) + "   ##\n\n";
        document.getElementById("result").innerHTML += files[i];
        document.getElementById("result").innerHTML += "\n\n##   End File   ##\n\n";
        configFiles.push(files[i]);
    }
}

/** 
 * Creates a config .cfg for Yealink phones
 * 
 * @param {string} mode the mode the user is in, either Individual or Bulk
 * @param {string} extension the phone's extension
 * @param {string} did the phone's DID number
 * @param {string} password the password for the phone's extension
 * @param {string} mac the MAC address of the phone
 * @param {string} timeZone the time zone the phone will be operating in
 * @param {string} background the background to use on the phone
 * @param {string} name the user name to be associated with the phone
 * @param {string} index the current index of the device row (Bulk only)
 * @returns {string} the config file
 */
function displayYealinkOutput(mode, extension, did, password, mac, timeZone, background, name, index = 0) {
    let file = "";
    file  = "#!version:1.0.0.1\n";
    file += "#--Config File Starts Here--#\n"
    file += "voice.handset.tone_vol = 12\n";
    file += "voice.handset.spk_vol = 7\n"
    file += "voice.handfree.tone_vol = 11\n";
    file += "voice.handfree.spk_vol = 15\n";
    file += "local_time.time_zone = " + timeZone + "\n";
    file += "local_time.ntp_server1 = time1.google.com\n";
    file += "local_time.ntp_server2 = time2.google.com\n";
    file += "local_time.interval = 15\n";
    file += "local_time.time_format = 0\n";
    file += "voice.tone.country = United States\n";
    file += "features.power_saving.enable = 0\n";
    file += "phone_setting.active_backlight_level = 10\n";
    file += "phone_setting.inactive_backlight_level = 0\n";
    // Outbound Extension
    file += "account.1.enable = 1\n";
    file += "account.1.label = " + did + "\n";
    file += "account.1.display_name = " + name + "\n";
    file += "account.1.user_name = " + extension + "\n";
    file += "account.1.auth_name = " + extension + "\n";
    file += "account.1.outbound_proxy_enable = 1\n";
    file += "account.1.sip_server.1.address = " + PROXY + "\n";
    file += "account.1.sip_server.1.expires = 30\n";
    file += "account.1.outbound_proxy.1.address = " + PROXY + "\n";
    file += "account.1.password = " + password + "\n";

    file += "features.config_dsskey_length = 2\n";
    file += "wallpaper_upload.url = " + SERVER_URL + "/pg/" + background + "\n";
    file += "phone_setting.backgrounds = Config:" + background + "\n";
    file += "static.auto_provision.server.url = " + SERVER_URL + "/xml/y" + mac + ".cfg\n";
    file += "static.network.dhcp_host_name = TT-" + extension + "\n";

    //	Line Settings
    const accountTable = document.getElementById("accounts");
    if (mode === "Individual") {
        let i = 2;
        for (let row; row = accountTable.rows[i-1]; i++) {
            file += "account." + i + ".enable = 1\n";
            file += "account." + i + ".label = " + row.cells[4].firstChild.value.trim() + "\n";
            file += "account." + i + ".display_name = " + row.cells[4].firstChild.value.trim() + "\n";
            file += "account." + i + ".user_name = " + row.cells[2].firstChild.value.trim() + "\n";
            file += "account." + i + ".auth_name = " + row.cells[2].firstChild.value.trim() + "\n";
            file += "account." + i + ".outbound_proxy_enable = 1\n";
            file += "account." + i + ".sip_server.1.address = " + PROXY + "\n";
            file += "account." + i + ".sip_server.1.expires = 30\n";
            file += "account." + i + ".outbound_proxy.1.address = " + PROXY + "\n";
            file += "account." + i + ".password = " + row.cells[3].firstChild.value.trim() + "\n";
        }
        file += clearExtraYealinkAccounts(i);
    } else {
        file += clearExtraYealinkAccounts(2);
    }
    
    // Add extra accounts and Speed Dials as linekeys
    if (mode === "Individual") {
        let linekeys = [];

        // Accounts
        for (let i = 1, row; row = accountTable.rows[i]; i++) {
            linekeys.push({
                keyNum: row.cells[1].firstChild.value,
                label: row.cells[4].firstChild.value.trim(), 
                line: i + 1, 
                type: 15, // Line
                value: 0
            });
        }
        // Speed Dials
        const sdTable = document.getElementById("speedDials");
        for (let i = 0, row; row = sdTable.rows[i]; i++) {
            linekeys.push({
                keyNum: row.cells[1].firstChild.value,
                label: row.cells[2].firstChild.value.trim(), 
                line: row.cells[4].firstChild.value.substring(4).trim(), 
                type: 13, // Speed Dial
                value: row.cells[3].firstChild.value.trim()
            });
        }
        linekeys = sortLineKeys(linekeys); // Puts line keys in order by key number

        // Cycles from line key 1 to the last set line key, adding them to the file
        // (Denoted by the linekeys[linekeys.length - 1].keyNum, which gets the keyNum of the last line key obj of the linekeys array)
        let j = 1;
        let keyNum = 0;
        for (; 0 < linekeys.length; j++) {
            keyNum = parseInt(linekeys[0].keyNum, 10);
            if (keyNum === j) {
                file += "linekey." + j + ".label = " + linekeys[0].label + "\n";
                file += "linekey." + j + ".line = " + linekeys[0].line + "\n";
                file += "linekey." + j + ".type = " + linekeys[0].type + "\n";
                file += "linekey." + j + ".value = " + linekeys[0].value + "\n";
                file += "linekey." + j + ".xml_phonebook = 0\n";
                linekeys.shift();
            } else {
                file += "linekey." + j + ".label = %NULL%\n";
                file += "linekey." + j + ".line = %NULL%\n";
                file += "linekey." + j + ".type = %NULL%\n";
                file += "linekey." + j + ".value = %NULL%\n";
                file += "linekey." + j + ".xml_phonebook = %NULL%\n";
            }
        }
        file += clearExtraYealinkKeys(j);
    } else {
        const deviceTable = document.getElementById("devices");
        let i = 0;
        let j = 1;
        for (let row; row = deviceTable.rows[i]; i++) {
            if (i === index) continue; // Skip self

            file += "linekey." + j + ".label = " + row.cells[7].firstChild.value.trim() + "\n";
            file += "linekey." + j + ".line = 1\n";
            file += "linekey." + j + ".type = 13\n"; // Speed Dial - 13
            file += "linekey." + j + ".value = " + row.cells[2].firstChild.value.trim() + "\n";
            file += "linekey." + j + ".xml_phonebook = 0\n";
            j++;
        }
        file += clearExtraYealinkKeys(i);
    }
    return file;
}

/** 
 * Sorts the takenKeys array in ascending order 
 * 
 * @param {Object[]} linekeys array of line key objects to sort by key number
 */
function sortLineKeys(linekeys) {
    linekeys.sort(function(a, b) {
        return a.keyNum - b.keyNum;
    });
    return linekeys;
}

/**
 * Sets all unused account fields to %NULL% to ensure no accounts from previous provisions hold over.
 * Essentially resets all unused accounts.
 * 
 * @param {number} start the account index to start at when clearing accounts
 */
function clearExtraYealinkAccounts(start) {
    let file = "";
    for (let i = start; i <= 16; i++) {
        file += "account." + i + ".enable = 0\n";
        file += "account." + i + ".label = %NULL%\n";
        file += "account." + i + ".display_name = %NULL%\n";
        file += "account." + i + ".user_name = %NULL%\n";
        file += "account." + i + ".auth_name = %NULL%\n";
        file += "account." + i + ".outbound_proxy_enable = %NULL%\n";
        file += "account." + i + ".sip_server.1.address = %NULL%\n";
        file += "account." + i + ".sip_server.1.expires = %NULL%\n";
        file += "account." + i + ".outbound_proxy.1.address = %NULL%\n";
        file += "account." + i + ".password = %NULL%\n";
    }
    return file;
}

/**
 * Sets all unused linekey fields to %NULL% to ensure no linekeys from previous provisions hold over.
 * Essentially resets all unused linekeys.
 * 
 * @param {number} start the linekey index to start at when clearing linekeys
 */
function clearExtraYealinkKeys(start) {
    let file = "";
    for (let i = start; i <= 27; i++) {
        file += "linekey." + i + ".label = %NULL%\n";
        file += "linekey." + i + ".line = %NULL%\n";
        file += "linekey." + i + ".type = %NULL%\n";
        file += "linekey." + i + ".value = %NULL%\n";
        file += "linekey." + i + ".xml_phonebook = %NULL%\n";
    }
    return file;
}

/** 
 * Creates a config .xml for Cisco phones
 * 
 * @param {string} mode the mode the user is in, either "Individual" or "Bulk"
 * @param {string} extension the phone's extension
 * @param {string} did the phone's DID number
 * @param {string} password the password for the phone's extension
 * @param {string} mac the MAC address of the phone
 * @param {string} timeZone the time zone the phone will be operating in
 * @param {string} background the background to use on the phone
 * @param {string} name the user name to be associated with the phone
 * @param {string} index the current index of the device row (Bulk only)
 * @returns {string} the config file
 */
function displayCiscoOutput(mode, extension, did, password, mac, timeZone, background, name, type, index = 0) {
    let file = "";

    // General Settings
    file  = "<flat-profile>\n";
    file += "<Connection_Type group=\"System/Internet_Connection_Type_\">DHCP</Connection_Type>\n";
    file += "<HostName group=\"System/Optional_Network_Configuration\">TTS-" + extension + "</HostName>\n";
    file += "<Domain group=\"Info/System_Information\">tourtech</Domain>\n";
    file += "<Primary_DNS group=\"System/Optional_Network_Configuration\">" + PRIMARY_DNS + "</Primary_DNS>\n";
    file += "<Secondary_DNS group=\"System/Optional_Network_Configuration\">" + SECONDARY_DNS + "</Secondary_DNS>\n";
    file += "<NTP_Enable group=\"Info/System_Information\">1</NTP_Enable>\n";
    file += "<DNS_Server_Order group=\"System/Optional_Network_Configuration\">Manual</DNS_Server_Order>\n";
    file += "<DNS_Query_Mode group=\"System/Optional_Network_Configuration\">Parallel</DNS_Query_Mode>\n";
    file += "<Primary_NTP_Server group=\"System/Optional_Network_Configuration\">time.nist.gov</Primary_NTP_Server>\n";
    file += "<Secondary_NTP_Server group=\"System/Optional_Network_Configuration\">time1.google.com</Secondary_NTP_Server>\n";
    file += "<Enable_CDP group=\"System/VLAN_Settings\">No</Enable_CDP>\n";
    file += "<SIP_TCP_Port_Min group=\"SIP/SIP_Parameters\">5060</SIP_TCP_Port_Min>\n";
    file += "<SIP_TCP_Port_Max group=\"SIP/SIP_Parameters\">5080</SIP_TCP_Port_Max>\n";
    file += "<Caller_ID_Header group=\"SIP/SIP_Parameters\">FROM</Caller_ID_Header>\n";
    file += "<Reg_Retry_Intvl group=\"SIP/SIP_Timer_Values__sec_\">30</Reg_Retry_Intvl>\n";
    file += "<Reg_Retry_Long_Intvl group=\"SIP/SIP_Timer_Values__sec_\">30</Reg_Retry_Long_Intvl>\n";
    file += "<RTP_Port_Min group=\"SIP/RTP_Parameters\">16384</RTP_Port_Min>\n";
    file += "<RTP_Port_Max group=\"SIP/RTP_Parameters\">16704</RTP_Port_Max>\n";
    file += "<RTP_Packet_Size group=\"SIP/RTP_Parameters\">0.020</RTP_Packet_Size>\n";
    file += "<Resync_Random_Delay group=\"Provisioning/Configuration_Profile\">1</Resync_Random_Delay>\n";
    file += "<Resync_Periodic group=\"Provisioning/Configuration_Profile\">0</Resync_Periodic>\n";
    file += "<Resync_Error_Retry_Delay group=\"Provisioning/Configuration_Profile\">3600</Resync_Error_Retry_Delay>\n";
    file += "<Resync_Fails_On_FNF group=\"Provisioning/Configuration_Profile\">Yes</Resync_Fails_On_FNF>\n";
    file += "<Profile_Rule group=\"Provisioning/Configuration_Profile\">" + SERVER_URL + "/xml/spa" + mac + ".xml</Profile_Rule>\n";
    file += "<Reorder_Delay group=\"Regional/Control_Timer_Values__sec_\">255</Reorder_Delay>\n";
    file += "<Set_Local_Date__mm_dd_ group=\"Regional/Time\"/>\n";
    file += "<Set_Local_Time__HH_mm_ group=\"Regional/Time\"/>\n";
    file += "<Time_Zone group=\"Regional/Time\">" + timeZone + "</Time_Zone>\n";
    file += "<Time_Offset__HH_mm_ group=\"Regional/Time\"/>\n";
    file += "<Daylight_Saving_Time_Rule group=\"Regional/Time\">start=3/13/7/2:0:0;end=11/6/7;save=1</Daylight_Saving_Time_Rule>\n";
    file += "<Daylight_Saving_Time_Enable group=\"Regional/Time\">Yes</Daylight_Saving_Time_Enable>\n";
    file += "<Language_Selection group=\"Regional/Language\">English-US</Language_Selection>\n";
    file += "<Locale group=\"Regional/Language\">en-US</Locale>\n";
    file += "<Station_Display_Name group=\"Phone/General\">" + name + "</Station_Display_Name>\n";
    file += "<Station_Name group=\"Phone/General\">" + did + "</Station_Name>\n";
    file += "<Voice_Mail_Number group=\"Phone/General\">*97</Voice_Mail_Number>\n";
    
    // Line Settings //
    // Main User
    file += "<Extension_1_ group=\"Phone/Line_Key_1\">1</Extension_1_>\n";
    file += "<Short_Name_1_ group=\"Phone/Line_Key_1\">" + name + "</Short_Name_1_>\n";
    file += "<Subscription_Expires_1_ group=\"Ext_1/Share_Line_Appearance\">1800</Subscription_Expires_1_>\n";
    file += "<NAT_Mapping_Enable_1_ group=\"Ext_1/NAT_Settings\">No</NAT_Mapping_Enable_1_>\n";
    file += "<NAT_Keep_Alive_Enable_1_ group=\"Ext_1/NAT_Settings\">No</NAT_Keep_Alive_Enable_1_>\n";
    file += "<Proxy_1_ group=\"Ext_1/Proxy_and_Registration\">" + PROXY + ":5060</Proxy_1_>\n";
    file += "<Use_Outbound_Proxy_1_ group=\"Ext_1/Proxy_and_Registration\">Yes</Use_Outbound_Proxy_1_>\n";
    file += "<Outbound_Proxy_1_ group=\"Ext_1/Proxy_and_Registration\">" + PROXY + ":5060</Outbound_Proxy_1_>\n";
    file += "<Use_OB_Proxy_In_Dialog_1_ group=\"Ext_1/Proxy_and_Registration\">Yes</Use_OB_Proxy_In_Dialog_1_>\n";
    file += "<Register_1_ group=\"Ext_1/Proxy_and_Registration\">Yes</Register_1_>\n";
    file += "<Register_Expires_1_ group=\"Ext_1/Proxy_and_Registration\">10</Register_Expires_1_>\n";
    file += "<Proxy_Fallback_Intvl_1_ group=\"Ext_1/Proxy_and_Registration\">10</Proxy_Fallback_Intvl_1_>\n";
    file += "<Display_Name_1_ group=\"Ext_1/Subscriber_Information\">" + extension + "</Display_Name_1_>\n";
    file += "<User_ID_1_ group=\"Ext_1/Subscriber_Information\">" + extension + "</User_ID_1_>\n";
    file += "<Password_1_ group=\"Ext_1/Subscriber_Information\">" + password + "</Password_1_>\n";
    file += "<Preferred_Codec_1_ group=\"Ext_1/Audio_Configuration\">G711u</Preferred_Codec_1_>\n";
    file += "<Use_Pref_Codec_Only_1_ group=\"Ext_1/Audio_Configuration\">Yes</Use_Pref_Codec_Only_1_>\n";
    file += "<Dial_Plan_1_ group=\"Ext_1/Dial_Plan\">\n";
    file += "(*xxxxxx|**xxxxxx|[3469]11|0|00|[2-9]xxxxxx|1xxx[2-9]xxxxxxS0|xxxxxxxxxxxx.)\n";
    file += "</Dial_Plan_1_>\n";

    // Extra users and speed dials
    if (mode === "Individual") { // Individual Setup
        let linekeys = [];
        // Ensures 1 Line Cisco Phones dont try and assign more than the base user account
        if (type != "C_502G" && type != "C_922" && type != "C_301") {
            // Sets user lines
            const accountTable = document.getElementById("accounts");
            for (let i = 1, row; row = accountTable.rows[i]; i++) {
                linekeys.push({
                    keyNum: row.cells[1].firstChild.value,
                    line: i + 1,
                    label: row.cells[4].firstChild.value.trim(),
                    extended_func: "",
                    display_name: row.cells[2].firstChild.value.trim(),
                    user_id: row.cells[2].firstChild.value.trim(),
                    password: row.cells[3].firstChild.value.trim()
                });
            }

            // Sets speed dials
            const sdTable = document.getElementById("speedDials");
            for (let i = 0, row; row = sdTable.rows[i]; i++) {
                let ext_did = row.cells[3].firstChild.value.replace(/-|\s/g,"");
                linekeys.push({
                    keyNum: row.cells[1].firstChild.value,
                    line: "Disabled",
                    label: row.cells[2].firstChild.value.trim(),
                    extended_func: "fnc=sd;sub=" + ext_did + "@" + PROXY + ":5060;ext=" + ext_did + "@" + PROXY + ":5060",
                    display_name: row.cells[2].firstChild.value.trim(),
                    user_id: "",
                    password: ""
                });
            }

            linekeys = sortLineKeys(linekeys); // Sorts the line keys by key number

            let i = 2;
            let keyNum = 0;
            for (; 0 < linekeys.length; i++) {
                keyNum = parseInt(linekeys[0].keyNum, 10);
                if (keyNum === i) {
                    file += "<Extension_" + i + "_ group=\"Phone/Line_Key_" + i + "\">" + linekeys[0].line + "</Extension_" + i + "_>\n";
                    file += "<Short_Name_" + i + "_ group=\"Phone/Line_Key_" + i + "\">" + linekeys[0].label + "</Short_Name_" + i + "_>\n";
                    file += "<Extended_Function_" + i + "_ group=\"Phone/Line_Key_" + i + "\">" + linekeys[0].extended_func + "</Extended_Function_" + i + "_>\n";
                    file += "<Subscription_Expires_" + i + "_ group=\"Ext_" + i + "/Share_Line_Appearance\">1800</Subscription_Expires_" + i + "_>\n";
                    file += "<NAT_Mapping_Enable_" + i + "_ group=\"Ext_" + i + "/NAT_Settings\">No</NAT_Mapping_Enable_" + i + "_>\n";
                    file += "<NAT_Keep_Alive_Enable_" + i + "_ group=\"Ext_" + i + "/NAT_Settings\">No</NAT_Keep_Alive_Enable_" + i + "_>\n";
                    file += "<Proxy_" + i + "_ group=\"Ext_" + i + "/Proxy_and_Registration\">" + PROXY + ":5060</Proxy_" + i + "_>\n";
                    file += "<Use_Outbound_Proxy_" + i + "_ group=\"Ext_" + i + "/Proxy_and_Registration\">Yes</Use_Outbound_Proxy_" + i + "_>\n";
                    file += "<Outbound_Proxy_" + i + "_ group=\"Ext_" + i + "/Proxy_and_Registration\">" + PROXY + ":5060</Outbound_Proxy_" + i + "_>\n";
                    file += "<Use_OB_Proxy_In_Dialog_" + i + "_ group=\"Ext_" + i + "/Proxy_and_Registration\">Yes</Use_OB_Proxy_In_Dialog_" + i + "_>\n";
                    file += "<Register_" + i + "_ group=\"Ext_" + i + "/Proxy_and_Registration\">Yes</Register_" + i + "_>\n";
                    file += "<Register_Expires_" + i + "_ group=\"Ext_" + i + "/Proxy_and_Registration\">10</Register_Expires_" + i + "_>\n";
                    file += "<Proxy_Fallback_Intvl_" + i + "_ group=\"Ext_" + i + "/Proxy_and_Registration\">10</Proxy_Fallback_Intvl_" + i + "_>\n";
                    file += "<Display_Name_" + i + "_ group=\"Ext_" + i + "/Subscriber_Information\">" + linekeys[0].display_name + "</Display_Name_" + i + "_>\n";
                    file += "<User_ID_" + i + "_ group=\"Ext_" + i + "/Subscriber_Information\">" + linekeys[0].user_id + "</User_ID_" + i + "_>\n";
                    file += "<Password_" + i + "_ group=\"Ext_" + i + "/Subscriber_Information\">" + linekeys[0].password + "</Password_" + i + "_>\n";
                    file += "<Preferred_Codec_" + i + "_ group=\"Ext_" + i + "/Audio_Configuration\">G711u</Preferred_Codec_" + i + "_>\n";
                    file += "<Use_Pref_Codec_Only_" + i + "_ group=\"Ext_" + i + "/Audio_Configuration\">Yes</Use_Pref_Codec_Only_" + i + "_>\n";
                    file += "<Dial_Plan_" + i + "_ group=\"Ext_" + i + "/Dial_Plan\">\n";
                    file += "(*xxxxxx|**xxxxxx|[3469]11|0|00|[2-9]xxxxxx|1xxx[2-9]xxxxxxS0|xxxxxxxxxxxx.)\n";
                    file += "</Dial_Plan_" + i + "_>\n";
                    linekeys.shift();
                } else {
                    file += "<Extension_" + i + "_ group=\"Phone/Line_Key_" + i + "\">Disabled</Extension_" + i + "_>\n";
                    file += "<Short_Name_" + i + "_ group=\"Phone/Line_Key_" + i + "\"></Short_Name_" + i + "_>\n";
                    file += "<Extended_Function_" + i + "_ group=\"Phone/Line_Key_" + i + "\"></Extended_Function_" + i + "_>\n";
                    file += "<Subscription_Expires_" + i + "_ group=\"Ext_" + i + "/Share_Line_Appearance\"></Subscription_Expires_" + i + "_>\n";
                    file += "<NAT_Mapping_Enable_" + i + "_ group=\"Ext_" + i + "/NAT_Settings\"></NAT_Mapping_Enable_" + i + "_>\n";
                    file += "<NAT_Keep_Alive_Enable_" + i + "_ group=\"Ext_" + i + "/NAT_Settings\"></NAT_Keep_Alive_Enable_" + i + "_>\n";
                    file += "<Proxy_" + i + "_ group=\"Ext_" + i + "/Proxy_and_Registration\"></Proxy_" + i + "_>\n";
                    file += "<Use_Outbound_Proxy_" + i + "_ group=\"Ext_" + i + "/Proxy_and_Registration\"></Use_Outbound_Proxy_" + i + "_>\n";
                    file += "<Outbound_Proxy_" + i + "_ group=\"Ext_" + i + "/Proxy_and_Registration\"></Outbound_Proxy_" + i + "_>\n";
                    file += "<Use_OB_Proxy_In_Dialog_" + i + "_ group=\"Ext_" + i + "/Proxy_and_Registration\"></Use_OB_Proxy_In_Dialog_" + i + "_>\n";
                    file += "<Register_" + i + "_ group=\"Ext_" + i + "/Proxy_and_Registration\"></Register_" + i + "_>\n";
                    file += "<Register_Expires_" + i + "_ group=\"Ext_" + i + "/Proxy_and_Registration\"></Register_Expires_" + i + "_>\n";
                    file += "<Proxy_Fallback_Intvl_" + i + "_ group=\"Ext_" + i + "/Proxy_and_Registration\"></Proxy_Fallback_Intvl_" + i + "_>\n";
                    file += "<Display_Name_" + i + "_ group=\"Ext_" + i + "/Subscriber_Information\"></Display_Name_" + i + "_>\n";
                    file += "<User_ID_" + i + "_ group=\"Ext_" + i + "/Subscriber_Information\"></User_ID_" + i + "_>\n";
                    file += "<Password_" + i + "_ group=\"Ext_" + i + "/Subscriber_Information\"></Password_" + i + "_>\n";
                    file += "<Preferred_Codec_" + i + "_ group=\"Ext_" + i + "/Audio_Configuration\"></Preferred_Codec_" + i + "_>\n";
                    file += "<Use_Pref_Codec_Only_" + i + "_ group=\"Ext_" + i + "/Audio_Configuration\"></Use_Pref_Codec_Only_" + i + "_>\n";
                    file += "<Dial_Plan_" + i + "_ group=\"Ext_" + i + "/Dial_Plan\"></Dial_Plan_" + i + "_>\n";
                }
            }
            file += clearExtraCiscoExtensions(i); 
        }
    } else { // Bulk Setup
        const deviceTable = document.getElementById("devices");
        let i = 0;
        let j = 2;
        for (let row; row = deviceTable.rows[i]; i++) {
            if (i === index) continue;
            // Sets speed dials
            file += "<Extension_" + j + "_ group=\"Phone/Line_Key_" + j + "\">" + j + "</Extension_" + j + "_>\n";
            file += "<Short_Name_" + j + "_ group=\"Phone/Line_Key_" + j + "\">" + row.cells[7].firstChild.value.trim() + "</Short_Name_" + j + "_>\n";
            file += "<Subscription_Expires_" + j + "_ group=\"Ext_" + j + "/Share_Line_Appearance\">1800</Subscription_Expires_" + j + "_>\n";
            file += "<NAT_Mapping_Enable_" + j + "_ group=\"Ext_" + j + "/NAT_Settings\">No</NAT_Mapping_Enable_" + j + "_>\n";
            file += "<NAT_Keep_Alive_Enable_" + j + "_ group=\"Ext_" + j + "/NAT_Settings\">No</NAT_Keep_Alive_Enable_" + j + "_>\n";
            file += "<Proxy_" + j + "_ group=\"Ext_" + j + "/Proxy_and_Registration\">" + PROXY + ":5060</Proxy_" + j + "_>\n";
            file += "<Use_Outbound_Proxy_" + j + "_ group=\"Ext_" + j + "/Proxy_and_Registration\">Yes</Use_Outbound_Proxy_" + j + "_>\n";
            file += "<Outbound_Proxy_" + j + "_ group=\"Ext_" + j + "/Proxy_and_Registration\">" + PROXY + ":5060</Outbound_Proxy_" + j + "_>\n";
            file += "<Use_OB_Proxy_In_Dialog_" + j + "_ group=\"Ext_" + j + "/Proxy_and_Registration\">Yes</Use_OB_Proxy_In_Dialog_" + j + "_>\n";
            file += "<Register_" + j + "_ group=\"Ext_" + j + "/Proxy_and_Registration\">Yes</Register_" + j + "_>\n";
            file += "<Register_Expires_" + j + "_ group=\"Ext_" + j + "/Proxy_and_Registration\">10</Register_Expires_" + j + "_>\n";
            file += "<Proxy_Fallback_Intvl_" + j + "_ group=\"Ext_" + j + "/Proxy_and_Registration\">10</Proxy_Fallback_Intvl_" + j + "_>\n";
            file += "<Display_Name_" + j + "_ group=\"Ext_" + j + "/Subscriber_Information\">" + row.cells[2].firstChild.value.trim() + "</Display_Name_" + j + "_>\n";
            file += "<User_ID_" + j + "_ group=\"Ext_" + j + "/Subscriber_Information\">" + row.cells[2].firstChild.value.trim() + "</User_ID_" + j + "_>\n";
            file += "<Password_" + j + "_ group=\"Ext_" + j + "/Subscriber_Information\">" + row.cells[4].firstChild.value.trim() + "</Password_" + j + "_>\n";
            file += "<Preferred_Codec_" + j + "_ group=\"Ext_" + j + "/Audio_Configuration\">G711u</Preferred_Codec_" + j + "_>\n";
            file += "<Use_Pref_Codec_Only_" + j + "_ group=\"Ext_" + j + "/Audio_Configuration\">Yes</Use_Pref_Codec_Only_" + j + "_>\n";
            file += "<Dial_Plan_" + j + "_ group=\"Ext_" + j + "/Dial_Plan\">\n";
            file += "(*xxxxxx|**xxxxxx|[3469]11|0|00|[2-9]xxxxxx|1xxx[2-9]xxxxxxS0|xxxxxxxxxxxx.)\n";
            file += "</Dial_Plan_" + j + "_>\n";
            j++;
        }
        file += clearExtraCiscoExtensions(j);
    }

    // Sound and Display settings
    file += "<Text_Logo group=\"Phone/General\">TOURtech</Text_Logo>\n";
    file += "<BMP_Picture_Download_URL group=\"Phone/General\">" + SERVER_URL + "/pg/" + background + "</BMP_Picture_Download_URL>\n";
    file += "<Select_Logo group=\"Phone/General\">Text Logo</Select_Logo>\n";
    file += "<Select_Background_Picture group=\"Phone/General\">BMP Picture</Select_Background_Picture>\n";
    file += "<Screen_Saver_Enable group=\"Phone/General\">No</Screen_Saver_Enable>\n";
    file += "<Screen_Saver_Wait group=\"Phone/General\">300</Screen_Saver_Wait>\n";
    file += "<Screen_Saver_Icon group=\"Phone/General\">Background Picture</Screen_Saver_Icon>\n";
    file += "<Ringer_Volume group=\"User/Audio_Volume\">8</Ringer_Volume>\n";
    file += "<Speaker_Volume group=\"User/Audio_Volume\">12</Speaker_Volume>\n";
    file += "<Handset_Volume group=\"User/Audio_Volume\">10</Handset_Volume>\n";
    file += "<Headset_Volume group=\"User/Audio_Volume\">10</Headset_Volume>\n";
    file += "<LCD_Contrast group=\"User/Audio_Volume\">8</LCD_Contrast>\n";
    file += "<Back_Light_Enable group=\"User/Screen\">Yes</Back_Light_Enable>\n";
    file += "<Back_Light_Timer__sec_ group=\"User/Screen\">30</Back_Light_Timer__sec_>\n";
    file += "</flat-profile>";

    return file;
}

/**
 * Clears unused extensions to ensure no line keys from previous provisions hold over.
 * Essentially resets all unused line keys.
 * 
 * @param {number} start which extension to start on when clearing
 */
function clearExtraCiscoExtensions(start) {
    let file = "";
    let maxExt = 5; // Max Cisco extensions accross all supported Cisco phones

    for (let i = start; i <= maxExt; i++) {
        file += "<Extension_" + i + "_ group=\"Phone/Line_Key_" + i + "\">Disabled</Extension_" + i + "_>\n";
        file += "<Short_Name_" + i + "_ group=\"Phone/Line_Key_" + i + "\"></Short_Name_" + i + "_>\n";
        file += "<Subscription_Expires_" + i + "_ group=\"Ext_" + i + "/Share_Line_Appearance\"></Subscription_Expires_" + i + "_>\n";
        file += "<NAT_Mapping_Enable_" + i + "_ group=\"Ext_" + i + "/NAT_Settings\"></NAT_Mapping_Enable_" + i + "_>\n";
        file += "<NAT_Keep_Alive_Enable_" + i + "_ group=\"Ext_" + i + "/NAT_Settings\"></NAT_Keep_Alive_Enable_" + i + "_>\n";
        file += "<Proxy_" + i + "_ group=\"Ext_" + i + "/Proxy_and_Registration\"></Proxy_" + i + "_>\n";
        file += "<Use_Outbound_Proxy_" + i + "_ group=\"Ext_" + i + "/Proxy_and_Registration\"></Use_Outbound_Proxy_" + i + "_>\n";
        file += "<Outbound_Proxy_" + i + "_ group=\"Ext_" + i + "/Proxy_and_Registration\"></Outbound_Proxy_" + i + "_>\n";
        file += "<Use_OB_Proxy_In_Dialog_" + i + "_ group=\"Ext_" + i + "/Proxy_and_Registration\"></Use_OB_Proxy_In_Dialog_" + i + "_>\n";
        file += "<Register_" + i + "_ group=\"Ext_" + i + "/Proxy_and_Registration\"></Register_" + i + "_>\n";
        file += "<Register_Expires_" + i + "_ group=\"Ext_" + i + "/Proxy_and_Registration\"></Register_Expires_" + i + "_>\n";
        file += "<Proxy_Fallback_Intvl_" + i + "_ group=\"Ext_" + i + "/Proxy_and_Registration\"></Proxy_Fallback_Intvl_" + i + "_>\n";
        file += "<Display_Name_" + i + "_ group=\"Ext_" + i + "/Subscriber_Information\"></Display_Name_" + i + "_>\n";
        file += "<User_ID_" + i + "_ group=\"Ext_" + i + "/Subscriber_Information\"></User_ID_" + i + "_>\n";
        file += "<Password_" + i + "_ group=\"Ext_" + i + "/Subscriber_Information\"></Password_" + i + "_>\n";
        file += "<Preferred_Codec_" + i + "_ group=\"Ext_" + i + "/Audio_Configuration\"></Preferred_Codec_" + i + "_>\n";
        file += "<Use_Pref_Codec_Only_" + i + "_ group=\"Ext_" + i + "/Audio_Configuration\"></Use_Pref_Codec_Only_" + i + "_>\n";
        file += "<Dial_Plan_" + i + "_ group=\"Ext_" + i + "/Dial_Plan\"></Dial_Plan_" + i + "_>\n";
    }
    return file;
}

/**
 * Saves the current config file to the user's system when the save file button is clicked.
 * 
 * @param {string} mode the mode the user is in, either 'Individual' or 'Bulk'
 */
function saveTextAsFile(mode) {  
    //////////////////////////////////////////////////////////////////////////// 
    // Uncomment when Build Config File button and output area are phased out //
    ////////////////////////////////////////////////////////////////////////////
    /*
    * if (mode === "individual") {
    *     displayOutput();
    * else {
    *     displayOutputs_Bulk();
    * }
    */

    for (let i = 0; i < configFiles.length; i++) {
        // grab the content of the form field and place it into a variable
        let textToWrite = configFiles[i];
        //  create a new Blob (html5 magic) that conatins the data from your form feild
        let textFileAsBlob = new Blob([textToWrite], {type:'application/xml'});

        // Specify the name of the file to be saved
        let phoneType = "";
        let barcode = "";
        if (mode === "Individual") { // Individual Setup
            phoneType = document.getElementById("phoneSelect").value;
            barcode = document.getElementById("Barcode").value;
        } else { // Bulk Setip
            phoneType = document.getElementsByClassName("phoneSelect_Bulk")[i].value;
            barcode = document.getElementsByClassName("Barcode_Bulk")[i].value;
        }
        let fileNameToSaveAs = "";
        switch (phoneType) {
            case "Y_T46G":
                fileNameToSaveAs = "y" + getMacAddress(barcode) + ".cfg";
                break;
            case "C_502G":
            case "C_922":
            case "C_301":
            case "C_504G":
            case "C_525G":
            case "C_525G2":
                fileNameToSaveAs = "spa" + getMacAddress(barcode) + ".xml";
                break;
        }

        // create a link for our script to 'click'
        let downloadLink = document.createElement("a");
        // supply the name of the file (from above).
        downloadLink.download = fileNameToSaveAs;
        // provide text for the link. This will be hidden so you
        // can actually use anything you want.
        downloadLink.innerHTML = "My Hidden Link";
            
        // allow our code to work in webkit & Gecko based browsers
        // without the need for a if / else block.
        window.URL = window.URL || window.webkitURL;
                
        // Create the link Object.
        downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
        // when link is clicked call a function to remove it from
        // the DOM in case user wants to save a second file.
        downloadLink.onclick = destroyClickedElement;
        // make sure the link is hidden.
        downloadLink.style.display = "none";
        // add the link to the DOM
        document.body.appendChild(downloadLink);
            
        // click the new link
        downloadLink.click();
    }
}

/** 
 * Remove the link from the DOM.
 */
function destroyClickedElement(event) {
    document.body.removeChild(event.target);
}

/** 
 * !!!!! TODO !!!!!
 * Uploads the given file to the VOIP phone server. 
 * 
 * @param {*} file the file to upload
 */
function uploadToServer(file) {
    return;
}