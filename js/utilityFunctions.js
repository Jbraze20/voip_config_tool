/** The maximum number of accounts a phone model can have.
 * Y - Yealink
 * C - Cisco
 */
const max_accounts = {
    /** 16 accounts */
    Y_T46G: 16,
    /** 1 accounts */
    C_502G: 1,
    /** 1 accounts */
    C_922: 1,
    /** 1 accounts */
    C_301: 1,
    /** 4 accounts */
    C_504G: 4,
    /** 5 accounts */
    C_525G: 5,
    /** 5 accounts */
    C_525G2: 5
};
/** The maximum number of keys a phone model can use. */
const max_keys = {
    /** 16 keys */
    Y_T46G: 27,
    /** 0 keys not including the user */
    C_502G: 0,
    /** 0 keys not including the user */
    C_922: 0,
    /** 0 keys not including the user */
    C_301: 0,
    /** 3 keys not including the user */
    C_504G: 3,
    /** 4 keys not including the user */
    C_525G: 4,
    /** 5 keys not including the user */
    C_525G2: 4
};

const Y_LINE_NUM_SELECT = "<select class=\"keyNumSelect\" data-cur-value=0 onchange=\"unlockKey(this.dataset.curValue);takeKey(this);updateKeyNumSelect()\">" +
                            "<option value=\"\" disabled selected>Choose Key to use</option>" +
                            "<option value=1>Key 1</option>" +
                            "<option value=2>Key 2</option>" +
                            "<option value=3>Key 3</option>" +
                            "<option value=4>Key 4</option>" +
                            "<option value=5>Key 5</option>" +
                            "<option value=6>Key 6</option>" +
                            "<option value=7>Key 7</option>" +
                            "<option value=8>Key 8</option>" +
                            "<option value=9>Key 9</option>" +
                            "<option value=10>Key 10</option>" +
                            "<option value=11>Key 11</option>" +
                            "<option value=12>Key 12</option>" +
                            "<option value=13>Key 13</option>" +
                            "<option value=14>Key 14</option>" +
                            "<option value=15>Key 15</option>" +
                            "<option value=16>Key 16</option>" +
                            "<option value=17>Key 17</option>" +
                            "<option value=18>Key 18</option>" +
                            "<option value=19>Key 19</option>" +
                            "<option value=20>Key 20</option>" +
                            "<option value=21>Key 21</option>" +
                            "<option value=22>Key 22</option>" +
                            "<option value=23>Key 23</option>" +
                            "<option value=24>Key 24</option>" +
                            "<option value=25>Key 25</option>" +
                            "<option value=26>Key 26</option>" +
                            "<option value=27>Key 27</option>" +
                        "</select>";

const C5_LINE_NUM_SELECT = "<select class=\"keyNumSelect\" data-cur-value=0 onchange=\"unlockKey(this.dataset.curValue);takeKey(this);updateKeyNumSelect()\">" +
                            "<option value=\"\" disabled selected>Choose Key to use</option>" + 
                            "<option value=2>Key 2</option>" +
                            "<option value=3>Key 3</option>" +
                            "<option value=4>Key 4</option>" +
                            "<option value=5>Key 5</option>" +
                        "</select>";

const C4_LINE_NUM_SELECT = "<select class=\"keyNumSelect\" data-cur-value=0 onchange=\"unlockKey(this.dataset.curValue);takeKey(this);updateKeyNumSelect()\">" +
                            "<option value=\"\" disabled selected>Choose Key to use</option>" +
                            "<option value=2>Key 2</option>" +
                            "<option value=3>Key 3</option>" +
                            "<option value=4>Key 4</option>" +
                        "</select>";

/**
 * Determines the max number of lines and keys available to the chosen phone model
 * 
 * @returns {Object} maxKeys: the max number of  accounts available, maxAccounts: the max number of keys available.
 */
function getModelMaximums(type) {
    let maxKey = max_keys[type];
    let maxAccount = max_accounts[type];

    return {
        maxKeys: maxKey,
        maxAccounts: maxAccount
    };
}

/**
 * !!!!! TODO !!!!!
 * Retrieves the matching MAC address from the MongoDB database.
 * 
 * @param {string} barcode the device's barcode
 * @returns {string} the MAC address corresponding to the devices barcode
 */
function getMacAddress(barcode) {
    return barcode;
}

/**
 * !!!!! TODO !!!!!
 * Retrieves the matching MAC addresses from the MongoDB database.
 * 
 * @param {string[]} barcodes the devices' barcodes
 * @returns {string[]} the MAC addresses corresponding to the devices' barcodes
 */
function getMacAddress_Bulk(barcodes) {
    return barcodes;
}

/**
 * Toggles the Line select section when switching between Yealink and Cisco phones.
 * 
 * @param {string} phoneType the identifier corresponding to phone model
 */
function modeChange(phoneType) {
    const sdLines = document.getElementById("speedDials");
    let row = sdLines.rows[0]

    switch (phoneType) {
        case "Y_T46G":
            row.cells[4].firstChild.disabled = false;
            sdLines.style.display = "table-row-group";
            row.cells[1].innerHTML = getLineNumSelect();
            break;
        case "C_502G":
        case "C_922":
        case "C_301":
            sdLines.style.display = "none"; // The single line phones don't have keys to program
            break;
        case "C_504G":
        case "C_525G":
        case "C_525G2":
            row.cells[4].firstChild.disabled = true; // Cisco phones can't decide which line to do a speed dial on
            sdLines.style.display = "table-row-group";
            row.cells[1].innerHTML = getLineNumSelect();
            break;
    }
}

/** Gets the string for the line num select dropdown for either Yealink or Cisco phones */
function getLineNumSelect() {
    const type = document.getElementById("phoneSelect").value;
    switch (type) {
        case "Y_T46G":
            return Y_LINE_NUM_SELECT;
        case "C_502G":
        case "C_922":
        case "C_301":
            return;
        case "C_504G":
            return C4_LINE_NUM_SELECT;
        case "C_525G":
        case "C_525G2":
            return C5_LINE_NUM_SELECT;
    }
}