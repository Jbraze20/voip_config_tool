/** Keeps track of number of line keys being set. */
let curKey = 1;
/** Keeps track of the number of accounts being used. */
let curAccount = 1;
/** Keeps track of which keys are taken */
let takenKeys = [];

/** 
 * Resets all fields. 
 * 
 * @param {string} mode the mode the user is in, either 'Individual' or 'Bulk'
 */
function resetFields(mode) {
    if (mode === "Individual") {
        resetIndividual();
        checkLines("Individual");
    } else {
        resetBulk();
        checkLines("Bulk");
    }
    
    document.getElementById("result").innerHTML = ""; // Should be removed when output area is phased out
}

/** Resets Individual Setup specific fields. */
function resetIndividual() {
    document.getElementById("Extension").value = "";
    document.getElementById("DID").value = "";
    document.getElementById("Password").value = "";
    document.getElementById("Barcode").value = "";
    document.getElementById("timeZoneSelect").value = "GMT-05:00";
    document.getElementById("Background").value = "tt.bmp";
    document.getElementById("Name").value = "";
    while (curAccount > 1) { // Delete all accounts
        deleteRow("account" + curAccount);
    }
    while (curKey > 1) { // Delete all but one Speed Dial 
        deleteRow("key" + curKey);
    }
    if (curKey < 1) { // Add the 1 default speed dial if it was deleted manually
        addKey();
    }
    document.getElementById("keyName").value = "";
    document.getElementById("destination").value = "";
    takenKeys.length = 0; // Empties the taken keys array
    updateKeyNumSelect();
    document.getElementsByClassName("keyNumSelect")[0].value = "";
}

/** Resets Bulk Setup specific fields. */
function resetBulk() {
    while (curKey > 1) {
        deleteRow("device" + curKey);
    }
    if (curKey < 1) { // Add the 1 default device if it was deleted manually
        addDevice();
    }

    document.getElementById("timeZoneSelect_Bulk").value = "GMT-05:00";
    document.getElementsByClassName("phoneSelect_Bulk")[0].value = "Y_T46G";
    document.getElementsByClassName("Extension_Bulk")[0].value = "";
    document.getElementsByClassName("DID_Bulk")[0].value = "";
    document.getElementsByClassName("Password_Bulk")[0].value = "";
    document.getElementsByClassName("Barcode_Bulk")[0].value = "";
    document.getElementsByClassName("Background_Bulk")[0].value = "tt.bmp";
    document.getElementsByClassName("Name_Bulk")[0].value = "";
    
}

/** 
 * Individual Setup
 * 
 * Adds a new account row to the account section. 
 */
function addAccount() {
    curAccount++;

    const acctTable = document.getElementById("accounts");
    let newRow = acctTable.insertRow();
    newRow.setAttribute("id", "account" + curAccount);
    let cellRemove = newRow.insertCell();
    cellRemove.innerHTML = "<button type=\"button\" class=\"removeLineButton\" id=\"account" + curAccount + "\" onClick=\"unlockKey(this.parentElement.nextElementSibling.firstChild.value);updateKeyNumSelect();deleteRow(this.id);checkLines('Individual')\"></button>";
    let cellLineNum = newRow.insertCell();
    cellLineNum.setAttribute("id", "keyNum");
    cellLineNum.innerHTML = getLineNumSelect();
    let cellExt = newRow.insertCell();
    cellExt.innerHTML = "<input type=\"text\" id=\"accountExt\" autofocus value = \"\"/>";
    let cellPass = newRow.insertCell();
    cellPass.innerHTML = "<input type=\"text\" id=\"YaccountPassword\" autofocus value = \"\"/>";
    let cellName = newRow.insertCell();
    cellName.innerHTML = "<input type=\"text\" id=\"YdisplayName\" autofocus value = \"\"/>";

    checkLines("Individual");
    updateLineSelect(true);
}

/** 
 * Individual Setup
 * 
 * Toggles if the Accounts section is visible to the user or not. 
 */
function toggleAccountsVisible(visible) {
    let acctSection = document.getElementById("accounts");

    if (visible) {
        acctSection.style.display = "table-row-group";
    } else {
        acctSection.style.display = "none";
    }
}

/** 
 * Individual Setup 
 * 
 * Adds a new speed dial row to the speed dial section.
 */
function addKey() {
    curKey++;

    const sdTable = document.getElementById("speedDials");
    let newRow = sdTable.insertRow()
    newRow.setAttribute("id", "keyRow" + curKey);
    let cellRemove = newRow.insertCell();
    cellRemove.innerHTML = "<button type=\"button\" class=\"removeLineButton\" id=\"key" + curKey + "\" onClick=\"unlockKey(this.parentElement.nextElementSibling.firstChild.value);updateKeyNumSelect();deleteRow(this.id);checkLines('Individual')\"></button>";
    let cellLineNum = newRow.insertCell();
    cellLineNum.setAttribute("id", "keyNum");
    cellLineNum.innerHTML = getLineNumSelect();
    let cellName = newRow.insertCell();
    cellName.innerHTML = "<input type=\"text\" id=\"keyName\" autofocus value = \"\"/>";
    let cellDest = newRow.insertCell();
    cellDest.innerHTML = "<input type=\"text\" id=\"destination\" autofocus value = \"\"/>";
    let cellLine = newRow.insertCell();

    let options = "";
    for (let i = 1; i <= curAccount; i++) {
        options += "<option value=\"Line" + i + "\">Line " + i + "</option>";
    }
    if (document.getElementById("phoneSelect").value.includes("Y")) {
        cellLine.innerHTML = "<select id=\"lineSelect\">" + options + "</select>";
    } else {
        cellLine.innerHTML = "<select id=\"lineSelect\" disabled>" + options + "</select>";
    }

    checkLines("Individual");

}

/**
 * Bulk Setup
 * 
 * Adds a device in the Devices section.
 */
function addDevice() {
    curKey++;
    const deviceTable = document.getElementById("devices");
    let newRow = deviceTable.insertRow()
    let cellRemove = newRow.insertCell();
    cellRemove.innerHTML = "<button type=\"button\" class=\"removeLineButton\" id=\"device" + curKey + "\" onClick=\"deleteRow(this.id);checkLines('Bullk')\"></button>";
    let cellPhoneType = newRow.insertCell();
    cellPhoneType.innerHTML = "<select class=\"phoneSelect_Bulk\" onchange=\"checkLines('Bulk')\" required>" +
        "<option value=\"Y_T46G\">Yealink T46G (27 lines)</option>" + 
        "<option value=\"C_502G\" disabled>Cisco 502G (1 line)</option>" + 
        "<option value=\"C_922\" disabled>Cisco 922 (1 line)</option>" + 
        "<option value=\"C_301\" disabled>Cisco 301 (1 line)</option>" + 
        "<option value=\"C_504G\">Cisco 504G (4 lines)</option>" + 
        "<option value=\"C_525G\">Cisco 525G (5 lines)</option>" + 
        "<option value=\"C_525G2\">Cisco 525G2 (5 lines)</option>" + 
        "</select>";
    let cellExt = newRow.insertCell();
    cellExt.innerHTML = "<input type=\"text\" class=\"Extension_Bulk\" autofocus value = \"\"/>";
    let cellDID = newRow.insertCell();
    cellDID.innerHTML = "<input type=\"text\" class=\"DID_Bulk\" autofocus value = \"\"/>";
    let cellPassword = newRow.insertCell();
    cellPassword.innerHTML = "<input type=\"text\" class=\"Password_Bulk\" autofocus value = \"\"/>";
    let cellBarcode = newRow.insertCell();
    cellBarcode.innerHTML = "<input type=\"text\" class=\"Barcode_Bulk\" autofocus value = \"\"/>";
    let cellBackground = newRow.insertCell();
    cellBackground.innerHTML = "<input type=\"text\" class=\"Background_Bulk\" autofocus value = \"tt.bmp\"/>";
    let cellName = newRow.insertCell();
    cellName.innerHTML = "<input type=\"text\" class=\"Name_Bulk\" autofocus value = \"\"/>";

    checkLines("Bulk");
}

/**
 * Checks if the selected phone model can support the current number of accounts and keys
 * for the Individual tab and checks if all devices in the Bulk tab have can support the 
 * overall number of devices.
 * 
 * @param {string} mode the mode the user is in, either 'Individual' or 'Bulk'
 */
 function checkLines(mode) {
    if (mode === "Individual") {
        let type = document.getElementById("phoneSelect").value;
        let modelMax = getModelMaximums(type);
    
        checkKeys(modelMax.maxKeys);
        checkAccounts(modelMax.maxAccounts, modelMax.maxKeys);
    } else {
        checkDevices();
    }
}

/** 
 * Checks the key lines' validity.
 * 
 * @param {number} maxKeys The maximum number of keys (extra accounts + speed dials) the current model supports.
 */
function checkKeys(maxKeys) {
    while (curKey > maxKeys && curKey > 1) {
        deleteRow("key" + curKey);
    }
    if (maxKeys === 0) {
        document.getElementById("keyRow1").cells[2].firstChild.value = "";
        document.getElementById("keyRow1").cells[3].firstChild.value = "";
        return;
    } 
    // Checking if the number of speed dial keys and additional accounts exceeds
    // the total number of keys on a phone model. The default account is not included.
    if ((curKey + curAccount - 1) >= maxKeys) {
        document.getElementById("addKeyButton").disabled = true;
    } else {
        document.getElementById("addKeyButton").disabled = false;
    }
}

/** 
 * Checks the Account lines' validity.
 * 
 * @param {number} maxAccounts The maximum number of accounts the current model supports.
 */
function checkAccounts(maxAccounts, maxKeys) {
    if (curAccount > 1) {
        toggleAccountsVisible(true);
        while (curAccount > maxAccounts) {
            deleteRow("account" + curAccount)
        }
    }
    if (curAccount === 1) { // No extra accounts, so don't show account section
        toggleAccountsVisible(false);
    }
    // Checking if the number of speed dial keys and additional accounts exceeds
    // the total number of keys on a phone model. The default account is not included.
    if (curAccount >= maxAccounts || (curKey + curAccount - 1) >= maxKeys) {
        document.getElementById("addAccountsButton").disabled = true;
    } else {
        document.getElementById("addAccountsButton").disabled = false;
    }
}

/** Checks the validity of the number of phones based on the key cap of the smallest device. */
function checkDevices() {
    let devices = document.getElementById("devices");
    let maxDeviceNum = 30; // Overshooting highest possible device num
    let maxKeys = 0;
    for (let i = 0, row; row = devices.rows[i]; i++) {
        maxKeys = getModelMaximums(row.cells[1].firstChild.value).maxKeys;
        // Cisco phones max keys do not include the one the user takes up.
        if (row.cells[1].firstChild.value[0] !== 'Y') {
            maxKeys++;
        }
        if (maxKeys < maxDeviceNum) {
            maxDeviceNum = maxKeys;
        }
    }
    if (curKey === maxDeviceNum) {
        document.getElementById("addDeviceBtn").disabled = true;
    } else {
        document.getElementById("addDeviceBtn").disabled = false;
    }

    updatePhoneSelect();
}

/** 
 * Updates the Line Select section in the speed dial section so that you can choose from available Accounts.
 * 
 * @param {boolean} add If true, add a line option, else, delete one.
 */
function updateLineSelect(add) {
    const sdLines = document.getElementById("speedDials");

    for (let i = 0, row; row = sdLines.rows[i]; i++) {
        if (add) {
            let option = document.createElement("option");
            option.text = "Line " + curAccount;
            option.value = "Line" + curAccount;
            row.cells[4].firstChild.add(option);
        } else {
            row.cells[4].firstChild.remove(curAccount);
        }
    }
}

/** 
 * Updates the Phone Select dropdown so that you can't choose a model with 
 * less line keys than the current number of devices. 
 */
function updatePhoneSelect() {
    let devices = document.getElementById("devices");
    if (devices.rows.length === 0) return; // All devices were deleted

    const optionNum = devices.rows[0].cells[1].firstChild.options.length;
    let optionValue = "";
    let maxKeys = 0;

    for (let i = 0, row; row = devices.rows[i]; i++) {
        for (let j = 0; j < optionNum; j++) {
            optionValue = row.cells[1].firstChild.options[j].value;
            maxKeys = getModelMaximums(optionValue).maxKeys;
            if (optionValue[0] !== 'Y') {
                maxKeys++;
            }
            if (maxKeys < curKey || maxKeys === 1) {
                row.cells[1].firstChild.options[j].disabled = true;
            } else {
                row.cells[1].firstChild.options[j].disabled = false;
            }
        }
    }
}

/** 
 * Adds a taken key value to the takenKeys array. 
 * 
 * @param {number} keyNum the key to take
 */
function takeKey(element) {
    takenKeys.push(element.value);
    sortKeys();
    element.dataset.curValue = element.value;
}

/** Sorts the takenKeys array in ascending order */
function sortKeys() {
    takenKeys.sort(function(a, b) {
        return a - b;
    });
}

/** 
 * Removes a freed up key from the takenKeys array. 
 * 
 * @param {number} keyNum the key number to remove from the takenKeys array
 */
function unlockKey(keyNum) {
    if (keyNum === 0) return; //No key was even selected

    takenKeys = takenKeys.filter(key => key !== keyNum)
}


/**
 * Updates the key selects so that the keys being used are greyed out, 
 * preventing a line key from being assigned to multiple lines or speed dials.
 * 
 * @param {HTMLSelectObject} element the calling select dropdown object
 */
function updateKeyNumSelect() {
    let accounts = document.getElementById("accounts");
    let speedDials = document.getElementById("speedDials");

    // Updates Account section
    for (let i = 1, row; row = accounts.rows[i]; i++) {
        let options = Array.from(row.cells[1].firstChild.options); // Gets array of options excluding the default option
        options.shift();
        options.forEach(option => {
            if (takenKeys.includes(option.value)) {
                option.disabled = true;
            } else {
                option.disabled = false;
            }
        });
    }
    // Updates Speed Dial section
    for (let i = 0, row; row = speedDials.rows[i]; i++) {
        let options = Array.from(row.cells[1].firstChild.options); // Gets array of options excluding the default option
        options.shift();
        options.forEach(option => {
            if (takenKeys.includes(option.value)) {
                option.disabled = true;
            } else {
                option.disabled = false;
            }
        });
    }
}

/** Copies the first background to all other devices. */
function copyBackgrounds() {
    let devices = document.getElementById("devices");
    if (devices.rows.length === 0) return; // All devices were deleted

    const background = devices.rows[0].cells[6].firstChild.value;

    for (let i = 1, row; row = devices.rows[i]; i++) {
        devices.rows[i].cells[6].firstChild.value = background;
    }
}

/**
 * Deletes the row of the specified table when the X button is clicked.
 * 
 * @param {string} row The ID of the row to be deleted.
 */
function deleteRow(row) {
    let rowParts = row.split(/(\d+)/).filter(Boolean);
    const lineNum = parseInt(rowParts[1], 10);
    let lineTable;

    if (rowParts[0] === "key") {
        lineTable = document.getElementById("speedDials");
        lineTable.deleteRow(lineNum - 1);
        curKey--;
    } else if (rowParts[0] === "account") {
        lineTable = document.getElementById("accounts");
        lineTable.deleteRow(lineNum - 1);
        curAccount--;
        updateLineSelect(false);
    } else {
        lineTable = document.getElementById("devices");
        lineTable.deleteRow(lineNum - 1);
        curKey--;
    }

    //Updates the remaining rows to accurately represent the number of lines in use
    if (rowParts[0] === "key" || rowParts[0] === "account") {
        for (let i = lineNum - 1, row; row = lineTable.rows[i]; i++) {
            row.id = rowParts[0] + "Row" + (i + 1);
            row.cells[0].id = rowParts[0] + (i + 1);
            row.cells[0].firstChild.id = rowParts[0] + (i + 1);
            row.cells[1].innerHTML = getLineNumSelect();
        }
    } else {
        for (let i = lineNum - 1, row; row = lineTable.rows[i]; i++) {
            row.cells[0].firstChild.id = rowParts[0] + (i + 1);
        }
    }
}